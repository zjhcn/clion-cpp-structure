//
// Created by hank on 2020/2/16.
//

#ifndef STRUCTURE_ARRAYQUEUE_H
#define STRUCTURE_ARRAYQUEUE_H

#include <iostream>
#include "./IQueue.h"
#include "../Array/Array.h"

using std::ostream;
using std::endl;

template<typename T>
class ArrayQueue : public IQueue<T> {
private:
    Array<T> array;

public:
    ArrayQueue() : ArrayQueue(5) {}
    ArrayQueue(int capacity) : array(capacity) {}

    ~ArrayQueue() {}

    friend ostream &operator<<(ostream &os, ArrayQueue &queue) {
        os << "Stack: front [";
        for (int i = 0; i < queue.array.getSize(); ++i) {
            os << queue.array[i];
            if (i != queue.array.getSize() - 1) {
                os << ", ";
            }
        }
        os << "] tail" << endl;
        return os;
    }

    void enqueue(T e) override {
        this->array.addLast(e);
    }

    T dequeue() override {
        return this->array.removeFirst();
    }

    T getFront() override {
        return this->array.getFirst();
    }

    int getSize() override {
        return this->array.getSize();
    }

    bool isEmpty() override {
        return this->array.isEmpty();
    }

};

#endif //STRUCTURE_ARRAYQUEUE_H
