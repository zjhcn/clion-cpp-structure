//
// Created by hank on 2020/2/16.
//

#ifndef STRUCTURE_LOOPQUEUE_H
#define STRUCTURE_LOOPQUEUE_H

#include <iostream>
#include <exception>
#include "./IQueue.h"

using std::invalid_argument;
using std::ostream;
using std::endl;

template<typename T>
class LoopQueue : public IQueue<T> {
private:
    T *array;
    int capacity;
    int front;
    int tail;

public:
    LoopQueue() : LoopQueue(5) {}

    LoopQueue(int capacity) {
        this->array = new T[capacity + 1];
        this->capacity = capacity;
        this->front = 0;
        this->tail = 0;
    }

    ~LoopQueue() {
        delete[] this->array;
    }

    friend ostream &operator<<(ostream &os, LoopQueue &queue) {
        os << "Queue: size = " << queue.getSize() << ", capacity = " << queue.capacity
           << endl
           << "front [";
        for (int i = queue.front; i != queue.tail; i = (i + 1) % queue.capacity) {
            os << queue.array[i];
            if ((i + 1) % queue.capacity != queue.tail) {
                os << ", ";
            }
        }
        os << "] tail"
           << endl;

        return os;
    }

    T getFront() override {
        if (this->isEmpty()) {
            throw invalid_argument("Error: Remove failed. Array is empty");
        }
        return this->array[this->front];
    }

    int getSize() override {
        return this->front < this->tail ? this->tail - this->front : this->tail + this->capacity - this->front;
    }

    bool isEmpty() override {
        return this->front == this->tail;
    }

    void enqueue(T e) override {
        if ((this->tail + 1) % this->capacity == this->front) {
            this->resize(this->capacity * 2);
        }

        this->array[this->tail] = e;
        this->tail = (this->tail + 1) % this->capacity;
    }

    T dequeue() override {
        if (this->isEmpty()) {
            throw invalid_argument("Error: Dequeue failed. Queue is empty");
        }

        T res = this->array[this->front];
        this->front = (this->front + 1) % this->capacity;

        if (this->capacity / 4 == this->getSize() && this->capacity / 2 != 0) {
            this->resize(this->capacity / 2);
        }

        return res;
    }

private:
    void resize(int newCapacity) {
        T *newArray = new T[newCapacity + 1];
        int size = this->getSize();
        for (int i = 0; i < size; i++) {
            newArray[i] = this->array[(this->front + i) % this->capacity];
        }

        delete[] this->array;
        this->front = 0;
        this->tail = size;
        this->array = newArray;
        this->capacity = newCapacity;
    }

};

#endif //STRUCTURE_LOOPQUEUE_H
