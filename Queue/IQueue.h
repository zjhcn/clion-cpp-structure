//
// Created by hank on 2020/2/16.
//

#ifndef STRUCTURE_IQUEUE_H
#define STRUCTURE_IQUEUE_H

template<typename T>
class IQueue {
public:
    virtual ~IQueue() {}

    virtual void enqueue(T e) = 0;

    virtual T dequeue() = 0;

    virtual T getFront() = 0;

    virtual int getSize() = 0;

    virtual bool isEmpty() = 0;
};

#endif //STRUCTURE_IQUEUE_H
