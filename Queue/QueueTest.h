//
// Created by hank on 2020/2/16.
//

#ifndef STRUCTURE_QUEUETEST_H
#define STRUCTURE_QUEUETEST_H

#include <cassert>
#include <ctime>
#include <string>
#include "../Student.h"
#include "IQueue.h"
#include "ArrayQueue.h"
#include "./LoopQueue.h"

using std::string;
using std::cout;
using std::endl;

namespace QueueTest {
    void ArrayQueueTest() {
        ArrayQueue<int> queue;
        queue.enqueue(1);
        queue.enqueue(2);
        queue.enqueue(3);
        queue.enqueue(4);
        cout << queue;

        queue.dequeue();
        cout << queue;

        queue.dequeue();
        cout << queue;

        queue.enqueue(5);
        cout << queue;
    }

    void LoopQueueTest() {
        int i = 0;
        LoopQueue<int> queue;
        queue.enqueue(i++);
        cout << queue << endl;
        queue.enqueue(i++);
        cout << queue << endl;
        queue.enqueue(i++);
        cout << queue << endl;
        queue.enqueue(i++);
        cout << queue << endl;
        queue.enqueue(i++);
        cout << queue << endl;
        queue.enqueue(i++);
        queue.enqueue(i++);
        queue.enqueue(i++);
        queue.enqueue(i++);
        queue.enqueue(i++);
        queue.enqueue(i++);
        cout << queue << endl;

        int ele = queue.dequeue();
        cout << "dequeue: " << ele << endl;

        while (queue.getSize() < 30) {
            queue.enqueue(i++);
        }
        cout << queue << endl;
        while (queue.getSize() > 10) {
            queue.dequeue();
        }
        cout << queue << endl;
    }

    void testQueue(IQueue<int> &queue, int n) {
        clock_t startTime = clock();
        srand(time(NULL));

        for (int i = 0; i < n; ++i) {
            queue.enqueue(rand() % 10);
        }
        for (int i = 0; i < n; ++i) {
            queue.dequeue();
        }
        clock_t endTime = clock();

        cout << "TEST time: " << double(endTime - startTime) / CLOCKS_PER_SEC << "s" << endl;
    }

    void compareArrayQueueLoopQueue() {
        int opCount = 10000 * 10;
        ArrayQueue<int> aq;
        cout << "ArrayQueue";
        testQueue(aq, opCount);
        LoopQueue<int> lq;
        cout << "LoopQueue";
        testQueue(lq, opCount);
    }

    void main() {
        cout << "================ Queue Test ===============" << endl;
        LoopQueueTest();
    }
}

#endif //STRUCTURE_QUEUETEST_H
