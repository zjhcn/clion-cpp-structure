//
// Created by hank on 2020/2/14.
//

#ifndef STRUTURE_IARRAY_H
#define STRUTURE_IARRAY_H

template<typename T>
class IArray {
private:
    T *data;
    int size;

public:
    virtual ~IArray() {}

    virtual int getSize() = 0;

    virtual int getCapacity() = 0;

    virtual bool isEmpty() = 0;

    virtual bool contains(T) = 0;

    virtual int find(T e) = 0;

    virtual T &get(int index) = 0;

    virtual T &getFirst() = 0;

    virtual T &getLast() = 0;

    virtual void resize(int index) = 0;

    virtual void add(int index, T e) = 0;

    virtual void addLast(T e) = 0;

    virtual void addFirst(T e) = 0;

    virtual T remove(int index) = 0;

    virtual T removeFirst() = 0;

    virtual T removeLast() = 0;

    virtual void removeElement(T e) = 0;

    virtual T &operator[](int index) = 0;

};

#endif //STRUTURE_IARRAY_H
