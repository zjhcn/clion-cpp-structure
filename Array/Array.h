//
// Created by hank on 2020/2/13.
//

#ifndef STRUTURE_ARRAY_H
#define STRUTURE_ARRAY_H

#include "IArray.h"
#include <iostream>
#include <exception>
#include <string>

using std::cout;
using std::endl;
using std::ostream;
using std::invalid_argument;
using std::string;
using std::to_string;

template<typename T>
class Array : public IArray<T> {
private:
    T *data;
    int capacity;
    int size;

public:
    Array() : Array(10) {}

    Array(int capacity) {
        this->capacity = capacity;
        this->size = 0;
        this->data = new T[capacity];
    }

    ~Array() override {
        delete[] this->data;
    }

    int getSize() override {
        return this->size;
    }

    int getCapacity() override {
        return this->capacity;
    }

    bool isEmpty() override {
        return this->size == 0;
    }

    int find(T e) override {
        for (int i = 0; i < this->size; ++i) {
            if (e == this->data[i]) {
                return i;
            }
        }

        return -1;
    }

    T &get(int index) override {
        if (index < 0 || index >= this->size) {
            throw invalid_argument("Error: Get failed. Index is invalid");
        }
        return this->data[index];
    }

    T &getFirst() override {
        return this->get(0);
    }

    T &getLast() override {
        return this->get(this->size - 1);
    }

    bool contains(T e) {
        return this->find(e) > -1;
    }

    void resize(int capacity) override {
        T *newData = new T[capacity];
        for (int i = 0; i < this->size; ++i) {
            newData[i] = this->data[i];
        }

        delete[] this->data;
        this->capacity = capacity;
        this->data = newData;
    }

    void add(int index, T e) override {
        if (this->size == this->capacity) {
            this->resize(this->capacity * 2);
        }
        if (index > this->size || index < 0) {
            throw invalid_argument("Error: Add failed. Require index >= 0 and index <= Array size.");
        }

        for (int i = this->size; i > index; i--) {
            this->data[i] = this->data[i - 1];
        }
        this->data[index] = e;
        this->size++;
    }

    void addFirst(T e) override {
        this->add(0, e);
    }

    void addLast(T e) override {
        this->add(this->size, e);
    }

    T remove(int index) override {
        if (this->isEmpty()) {
            throw invalid_argument("Error: Remove failed. Array is empty");
        }
        if (index >= this->size || index < 0) {
            throw invalid_argument("Error: Remove failed. Index is invalid.");
        }
        T res = this->data[index];
        for (int i = index; i < this->size; ++i) {
            this->data[i] = this->data[i + 1];
        }
        this->size--;

        if (this->size == this->capacity / 4 && this->capacity / 2 != 0) {
            this->resize(this->capacity / 2);
        }
        return res;
    }

    T removeFirst() override {
        return this->remove(0);
    }

    T removeLast() override {
        return this->remove(this->size - 1);
    }

    void removeElement(T e) {
        int index = this->find(e);
        if (index != -1) {
            this->remove(index);
        }
    }

    T &operator[](int index) {
        if (index >= this->size || index < 0) {
            throw invalid_argument("Error: Get failed. Index is illegal.");
        }
        return this->data[index];
    }

    friend ostream &operator<<(ostream &os, Array arr) {
        os << "Array: size = "
           << arr.size
           << ", capacity = "
           << arr.capacity
           << endl

           << "[";
        for (int i = 0; i < arr.size; ++i) {
            os << arr.data[i];
            if (i + 1 != arr.size) {
                os << ", ";
            }
        }
        os << "]"
           << endl;

        return os;
    }

};


#endif //STRUTURE_ARRAY_H
