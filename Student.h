//
// Created by hank on 2020/2/11.
//

#ifndef C__AC_STUDENT_H
#define C__AC_STUDENT_H

#include <iostream>
#include <string>

using std::string;
using std::ostream;
using std::endl;

class Student {
public:
    Student() {}

    Student(string name, int score) {
        this->name = name;
        this->score = score;
    }

    Student(const Student &another) {
        this->name = another.name;
        this->score = another.score;
    }

    bool operator<(const Student &otherStudent) {
        return this->score == otherStudent.score ? this->score < otherStudent.score : this->name < otherStudent.name;
    };

    bool operator>(const Student &otherStudent) {
        return this->score == otherStudent.score ? this->score > otherStudent.score : this->name > otherStudent.name;
    }

    bool operator==(const Student &another) {
        return this->name == another.name && this->score == another.score;
    }

    friend ostream &operator<<(ostream &os, const Student &student) {
        os << "Student: " << student.name << " " << student.score;
        return os;
    }

private:
    string name;
    int score;
};

#endif //C__AC_STUDENT_H
