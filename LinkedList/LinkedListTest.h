//
// Created by hank on 2020/2/16.
//

#ifndef STRUCTURE_LINKEDLISTTEST_H
#define STRUCTURE_LINKEDLISTTEST_H

#include <cassert>
#include <ctime>
#include <string>
#include "../Student.h"
#include "ILinkedList.h"
#include "LinkedList.h"

using std::string;
using std::cout;
using std::endl;

namespace LinkedListTest {
    void LinkedListTest1() {
        LinkedList<int> link;
        link.addFirst(1);
        link.add(0, 0);
        link.addLast(2);
    }

    void main() {
        cout << "================ LinkedList Test ===============" << endl;
        LinkedListTest1();
    }
}

#endif //STRUCTURE_LINKEDLISTTEST_H
