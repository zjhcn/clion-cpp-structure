//
// Created by hank on 2020/2/16.
//

#ifndef STRUCTURE_ILINKEDLIST_H
#define STRUCTURE_ILINKEDLIST_H

template<typename T>
class Node {
public:
    T e;
    Node *next;

public:
    Node(T e, Node *next) : e(e), next(next) {}
    Node(T e) :Node(e, nullptr) {}
    Node() : e(), next(nullptr) {}

    ~Node() {}
};

template<typename T>
class ILinkedList {
public:
    virtual ~ILinkedList() {}

    virtual int getSize() = 0;

    virtual bool isEmpty() = 0;

    virtual void addFirst(T e) = 0;

    virtual void addLast(T e) = 0;

    virtual void add(int index, T e) = 0;
};

#endif //STRUCTURE_ILINKEDLIST_H
