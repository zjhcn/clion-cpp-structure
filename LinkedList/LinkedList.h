//
// Created by hank on 2020/2/16.
//

#ifndef STRUCTURE_LINKEDLIST_H
#define STRUCTURE_LINKEDLIST_H

#include <iostream>
#include <exception>
#include "ILinkedList.h"

using std::invalid_argument;
using std::ostream;
using std::endl;

template<typename T>
class LinkedList : public ILinkedList<T> {
private:
    int size;
    Node<T> *dummyHead;

public:
    LinkedList() {
        this->size = 0;
        this->dummyHead = new Node<T>;
    }

    ~LinkedList() {}

    int getSize() override {
        return this->size;
    };

    bool isEmpty() override {
        return this->size == 0;
    }

    void addFirst(T e) override {
        this->add(0, e);
    }

    void addLast(T e) override {
        this->add(this->size, e);
    }

    void add(int index, T e) override {
        if (index < 0 || index > size) {
            throw invalid_argument("Error: Add failed. Index is invalid");
        }

        Node<T> *prev = this->dummyHead;
        for (int i = 0; i < index; ++i) {
            prev = prev->next;
        }

        prev->next = new Node<T>(e, prev->next);
        this->size++;
    }
};

#endif //STRUCTURE_LINKEDLIST_H
