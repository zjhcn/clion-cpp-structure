//
// Created by hank on 2020/2/15.
//

#ifndef STRUCTURE_ISTACK_H
#define STRUCTURE_ISTACK_H

template<typename T>
class IStack {
public:
    virtual ~IStack() {};

    virtual void push(T e) = 0;

    virtual T pop() = 0;

    virtual T peek() = 0;

    virtual int getSize() = 0;

    virtual bool isEmpty() = 0;
};

#endif //STRUCTURE_ISTACK_H
