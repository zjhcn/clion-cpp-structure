//
// Created by hank on 2020/2/15.
//

#ifndef STRUCTURE_STACKTEST_H
#define STRUCTURE_STACKTEST_H

#include <cassert>
#include <string>
#include "IStack.h"
#include "ArrayStack.h"
#include "../Student.h"

using std::string;
using std::cout;
using std::endl;

namespace StackTest {
    bool solution1(string str, IStack<char> *stack) {
        for (int i = 0; i < str.size(); ++i) {
            char c = str[i];
            if (c == '(' || c == '[' || c == '{') {
                stack->push(c);
            } else {
                if (stack->isEmpty()) {
                    return false;
                }
                switch (stack->pop()) {
                    case '(':
                        if (c != ')') {
                            cout << c;
                            return false;
                        }
                        break;
                    case '[':
                        if (c != ']') return false;
                        break;
                    case '{':
                        if (c != '}') return false;
                        break;
                }
            }
        } // end for

        return stack->isEmpty();
    }

    void test1() {
        ArrayStack<int> stack;
        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);
        cout << stack;
        cout << stack.peek() << endl;
        cout << stack;
        cout << stack.pop() << endl;
        cout << stack;
    }

    void test2() {
        ArrayStack<Student> stack;
        stack.push(Student("hank1", 1));
        stack.push(Student("hank2", 2));
        stack.push(Student("hank3", 3));
        stack.push(Student("hank4", 4));
        stack.push(Student("hank5", 5));
        cout << stack;
        cout << stack.peek() << endl;
        cout << stack;
        cout << stack.pop() << endl;
        cout << stack;
    }

    void test3() {
        assert(solution1("()", new ArrayStack<char>));
        assert(solution1("(){}[]", new ArrayStack<char>()));
        assert(solution1("({})", new ArrayStack<char>()));
        assert(!solution1("([", new ArrayStack<char>()));
        assert(!solution1("([)]", new ArrayStack<char>()));
        assert(solution1("(([]){})", new ArrayStack<char>()));

        cout << "solution1 括号匹配 TEST SUCCESS" << endl;
    }

    void main() {
        cout << "================ Stack Test ===============" << endl;
        test3();
    }
};


#endif //STRUCTURE_STACKTEST_H
