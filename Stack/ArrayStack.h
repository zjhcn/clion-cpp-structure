//
// Created by hank on 2020/2/15.
//

#ifndef STRUCTURE_ARRAYSTACK_H
#define STRUCTURE_ARRAYSTACK_H

#include <iostream>
#include "IStack.h"
#include "../Array/Array.h"

using std::ostream;
using std::endl;

template<typename T>
class ArrayStack : public IStack<T> {
private:
    Array<T> array;

public:
    ArrayStack() : array() {}

    ArrayStack(int capacity) : array(capacity) {}

    ~ArrayStack() {}

    void push(T e) override {
        this->array.addLast(e);
    }

    T pop() override {
        return this->array.removeLast();
    }

    T peek() override {
        return this->array.getLast();
    }

    int getSize() override {
        return this->array.getSize();
    }

    bool isEmpty() override {
        return this->array.isEmpty();
    }

    friend ostream &operator<<(ostream &os, ArrayStack &stack) {
        os << "Stack: [";
        for (int i = 0; i < stack.array.getSize(); ++i) {
            os << stack.array[i];
            if (i != stack.array.getSize() - 1) {
                os << ", ";
            }
        }
        os << "] top" << endl;
        return os;
    }

};

#endif //STRUCTURE_ARRAYSTACK_H
